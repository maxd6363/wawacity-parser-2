import time

from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from seleniumbase import Driver, SB
import sys

XPATH_LINK = '//*[@id="protected-container"]/div[2]/div/ul/li/a'

class Parser:
    def __init__(self, show_logs=False):
        self.show_logs = show_logs
        self.log(f"Init Driver")
    
    def log(self, msg):
        """
        Print message
        :param msg:
        :return:
        """
        if self.show_logs:
            print(msg)
    
    def dl_protect(self, url):
        """
        Download link from dl-protect
        :param url:
        :return:
        """
        
        with SB(uc=True, headless=False) as sb:
            self.log(f"Goto {url}")
        
            sb.driver.minimize_window()
            sb.driver.get(url)
            
            self.log(f"Wait 2.1s")
            sb.sleep(5.1)
            
            if sb.is_text_visible('Vérification en cours...', '#subButton'):
                self.log(f"Try new driver")
                sb.get_new_driver(undetectable=True)
                sb.driver.get(url)
                self.log(f"Wait 2.1s")
                sb.sleep(2.1)
                
                if sb.is_text_visible('Vérification en cours...', '#subButton'):
                    self.log(f"Try to find iframe")
                    if sb.is_element_visible('iframe[src*="challenge"]'):
                        with sb.frame_switch('iframe[src*="challenge"]'):
                            sb.click("span.mark")
                            
                            self.log(f"Wait 2.1s")
                            sb.sleep(2.1)
                            
            self.log(f"Activate demo mode")
            sb.activate_demo_mode()
            
            self.log(f"Clic on subButton")
            element = sb.find_element(By.ID, "subButton")
            sb.execute_script("arguments[0].click();", element)
            
            try:
                self.log("Try to find link")
                link = sb.find_element(By.XPATH, XPATH_LINK)
                if link:
                    href = link.get_attribute('href')
                    self.log(f"Find link {href}")
                    sb.driver.close()
                    return href
                else:
                    self.log("Error no link found")
                    sb.driver.save_screenshot('screen.png')
                    sb.driver.close()
                    raise Exception("Error no link found")
            except Exception as e:
                self.log(f"Error: {e}")
                sb.driver.save_screenshot('screen.png')
                sb.driver.close()
                raise e
    
if __name__ == '__main__':
    # we check if we have the url as argument
    if len(sys.argv) != 2:
        print("Error: You need to give the url as argument")
        sys.exit(1)


    # we get the first argument as url
    url = sys.argv[1]
    parser = Parser(show_logs=True)
    link = parser.dl_protect(url)
    print(f"<LINK>{link}<LINK/>")
    