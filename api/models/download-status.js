const downloadStatus = {
    UNKNOWN: "Unknown",
    STARTING: "Starting",
    GETTINGLINKS: "Getting Links",
    BYPASSINGPROTECTION: "Bypassing Protection",
    BYPASSINGPROTECTIONFAILED : "Bypassing Protection Failed",
    UNBLOCKING : "Unblocking",
    UNBLOCKINGFAILED : "Unblocking Failed",
    DOWNLOADING : "Downloading",
    COMPLETED : "Completed",
    FAILED : "Failed"
};

module.exports = downloadStatus;