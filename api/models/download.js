export class Download {
    id;
    url;
    title;
    quality;
    status;
    createdAt;
    updatedAt;
    progress;
    size;
  }
  