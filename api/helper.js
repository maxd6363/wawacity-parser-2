const axios = require('axios');
const cheerio = require('cheerio');
const env = require('./env.json');
const RealDebridClient = require('node-real-debrid');
const RD = new RealDebridClient(env.realDebrid);
const { spawn } = require('child_process');
const wget = require('wget-improved');


const getItems = (search, category, year, quality, count = 10) => {
    let amout = 0;

    console.log("Getting items for ", search, category, year, quality, count);

    if (quality === '4k') quality = 'ultra-hd-4k';
    else if (quality === '1080p' || quality === '720p') quality = 'blu-ray_1080p-720p';
    else quality = null;

    const url = `${env.wawacity}?p=${category}&search=${search}${year ? `&year=${year}` : ''}${quality ? `&s=${quality}` : ''}`;

    console.log("URL: ", url);

    return new Promise((resolve, reject) => {

        axios.get(url, { headers: { 'User-Agent': 'Mozilla/5.0' } })
            .then((response) => {
                const $ = cheerio.load(response.data);
                let result = [];

                $('.wa-sub-block-title').each((i, element) => {
                    if (amout >= count) {
                        return;
                    }

                    $(element).find('a').each((i, element2) => {
                        let title = $(element2).text();
                        let match = title.match(/^(.*?)\s*\[(.*?)\]/);
                        let quality = null;
                        let image = `${env.wawacity}${$(element).parent().find('img').attr('src')}`;

                        if (match) {
                            title = match[1].trim();
                            quality = match[2].trim();
                        }


                        let url = $(element2).attr('href');

                        let year = $(element2).parent().parent().find('span:contains("Année:")').next().text();

                        let language = $(element2).find('i.flag')?.attr('class');
                        if (language) {
                            language = language.replace('flag flag-', '').toUpperCase();

                            switch (language) {
                                case 'FR': language = '🇫🇷'; break;
                                case 'EN': language = '🇬🇧'; break;
                                case 'VOSTFR': language = '🇬🇧🇫🇷'; break;
                                case 'MULTI': language = '🇪🇺'; break;
                                default: language = '🌐'; break;
                            }
                        }

                        console.log("Title: ", title, "Year: ", year, "URL: ", url, "Quality: ", quality, "Image: ", image, "Language: ", language);

                        result.push({
                            title: title ?? 'unknown',
                            year: year ?? 'unknown',
                            url: url ?? 'unknown',
                            quality: quality ?? 'unknown',
                            image: image ?? 'https://i.ibb.co/4KhwwPH/image-2024-01-29-200250317.png',
                            language: language ?? 'unknown',
                        });
                        amout++;
                    });
                });
                resolve(result);
            })
            .catch((error) => {
                reject(error);
            });
    });
};

const getDownloadLinks = (item) => {
    return new Promise((resolve, reject) => {
        console.log("Getting download links for ", item.title);

        axios.get(`${env.wawacity}${item.url}`, { headers: { 'User-Agent': 'Mozilla/5.0' } })
            .then((response) => {
                const $ = cheerio.load(response.data);
                let result = [];

                $('.link-row').each((i, element) => {
                    if ($(element).find('td:contains("1fichier")').length > 0) {
                        let url = $(element).find('a').attr('href');
                        let title = $(element).find('td').first().find('a');
                        title.find('b').remove();
                        title = title.text().trim();

                        result.push({ title, url });
                    }
                });

                resolve(result);
            })
            .catch((error) => {
                reject(error);
            });
    });
}

const processDlProtect = (item) => {
    return new Promise((resolve, reject) => {
        console.log("Processing dl-protect for ", item.url);
        const python = spawn(env.pythonExecutable, ['dlprotect.py', item.url]);

        python.stdout.on('data', (data) => {
            const result = `${data}`;
            console.log(`<PYTHON>${result}`);

            const match = result.match(/<LINK>(.*?)<LINK\/>/);
            if (match) {
                resolve(match[1]);
            }
        });

        python.stderr.on('data', (data) => {
            console.log(`<PYTHON>ERROR: ${data}`);
            reject(data.toString());
        });
    });
}

const getDebridLink = (item) => {
    return new Promise(async (resolve, reject) => {
        try {
            const info = await RD.unrestrict.link(item.url);
            console.log("Got debrid link: ", info);
            resolve({ filename: info.filename, url: info.download, size: humanFileSize(info.filesize) });
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });
};

const downloadFile = (item, callback) => {
    return new Promise((resolve, reject) => {
        let lastProgress = 0;
        let download = wget.download(item.url, item.filename, {});
        download.on('error', function (err) {
            console.log(err);
            reject(err);
        });
        download.on('start', function (fileSize) {
            console.log("Start downloading filename: ", item.filename, "Size: ", humanFileSize(fileSize));
        });
        download.on('end', function (output) {
            console.log("Downloaded file: ", output);
            resolve(output);
        });
        download.on('progress', function (progress) {
            // if we passed to the next percent
            if (Math.floor(progress * 100) > lastProgress) {
                lastProgress = Math.floor(progress * 100);
                console.log(lastProgress);
                if (callback)
                    callback(lastProgress);
            }
        });
    });
};

function humanFileSize(size) {
    var i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
    return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

module.exports = { getItems, getDownloadLinks, getDebridLink, downloadFile, processDlProtect };
