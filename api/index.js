const express = require('express');
const cors = require('cors')
const { getItems, getDownloadLinks, processDlProtect, getDebridLink, downloadFile } = require('./helper');
const { v4 : uuid } = require('uuid');
const downloadStatus = require('./models/download-status');

const app = express();
const expressWs = require('express-ws')(app);

let webSocket = null;

let downloads = [];

app.use(express.json());
app.use(cors({
    origin: '*'
}));

app.get('/search', (req, res) => {
    const search = req.query.search;
    const category = req.query.category;

    //res.json([{"title":"Star Wars: The Clone Wars","year":"2008","url":"?p=film&id=20254-star-wars-the-clone-wars","quality":"BDRIP","image":"https://www.wawacity.city//img/films/e3231a71f9e80c7a91c34bf3cc207a97.webp","language":"🇫🇷"},{"title":"Star Wars: The Clone Wars","year":"2008","url":"?p=film&id=20255-star-wars-the-clone-wars","quality":"HDLIGHT 1080p","image":"https://www.wawacity.city//img/films/5d76fc6f298dffbcd7f8efd1bfae9b77.webp","language":"🇪🇺"},{"title":"Rogue One: A Star Wars Story","year":"2016","url":"?p=film&id=552-rogue-one-a-star-wars-story","quality":"HDTS MD","image":"https://www.wawacity.city//img/films/2d1640aafb3ea894ccb22ff6a825c4e0.webp","language":"🇬🇧"},{"title":"Star Wars : Episode I - La menace fantôme","year":"1999","url":"?p=film&id=748-star-wars-episode-i-la-menace-fant-me","quality":"HD-LIGHT 720p","image":"https://www.wawacity.city//img/films/6f50a387f61aa4f276e5572d81cf19c9.webp","language":"🇫🇷"},{"title":"Rogue One: A Star Wars Story","year":"2016","url":"?p=film&id=1006-rogue-one-a-star-wars-story","quality":"BRRIP MD","image":"https://www.wawacity.city//img/films/d0fd4fe9504916e9ae6f309b096ce1a6.webp","language":"🇫🇷"},{"title":"Rogue One: A Star Wars Story","year":"2016","url":"?p=film&id=1018-rogue-one-a-star-wars-story","quality":"Blu-Ray 720p","image":"https://www.wawacity.city//img/films/15cba5b268b8c6658eeedd34bbcd921d.webp","language":"🇫🇷"},{"title":"Rogue One: A Star Wars Story","year":"2016","url":"?p=film&id=1020-rogue-one-a-star-wars-story","quality":"BDRIP","image":"https://www.wawacity.city//img/films/5599da72324c1532b16c1bb4bdd95424.webp","language":"🇫🇷"},{"title":"Rogue One: A Star Wars Story","year":"2016","url":"?p=film&id=1189-rogue-one-a-star-wars-story","quality":"BDRIP","image":"https://www.wawacity.city//img/films/d0dd67015625d7d51c211193b3acf6c0.webp","language":"🇫🇷"},{"title":"Rogue One: A Star Wars Story","year":"2016","url":"?p=film&id=1190-rogue-one-a-star-wars-story","quality":"Blu-Ray 720p","image":"https://www.wawacity.city//img/films/55fd4f289456730c58679b0065898ec3.webp","language":"🇪🇺"},{"title":"Star Wars - Les Derniers Jedi","year":"2017","url":"?p=film&id=2936-star-wars-les-derniers-jedi","quality":"TS MD","image":"https://www.wawacity.city//img/films/496484695188d8734cf7f4ffa5a0f0fb.webp","language":"🇫🇷"}]);

    getItems(search, category).then((items) => {
        console.log(items);
        res.json(items);
    }).catch((err) => {
        res.status(500).json({ error: err.message });
    });
});

app.post('/download', (req, res) => {
    const download = req.body;
    console.log(download);

    download.id = uuid();
    download.status = downloadStatus.GETTINGLINKS;
    download.progress = 0;
    download.size = 0;
    download.createdAt = new Date();
    download.updatedAt = new Date();

    downloads.push(download);

    const response = {
        new : true,
        download: download
    };

    webSocket.send(JSON.stringify(response));
    res.status(200).json();

    getDownloadLinks(download).then((links) => {
        download.status = downloadStatus.BYPASSINGPROTECTION;
        download.url = links[0].url;
        download.updatedAt = new Date();

        const response = {
            update : true,
            download: download
        };

        webSocket.send(JSON.stringify(response));

        processDlProtect(download).then((url) => {
            download.status = downloadStatus.UNBLOCKING;
            download.url = url;
            download.updatedAt = new Date();

            const response = {
                update : true,
                download: download
            };

            webSocket.send(JSON.stringify(response));

            getDebridLink(download).then((file) => {
                download.status = downloadStatus.DOWNLOADING;
                download.url = file.url;
                download.size = file.size;
                download.filename = file.filename;
                download.updatedAt = new Date();

                const response = {
                    update : true,
                    download: download
                };

                webSocket.send(JSON.stringify(response));

                downloadFile(download, (progress) => {
                    download.status = downloadStatus.DOWNLOADING;
                    download.updatedAt = new Date();
                    download.progress = progress;

                    const response = {
                        update : true,
                        download: download
                    };

                    webSocket.send(JSON.stringify(response));
                }).then(() => {
                    download.status = downloadStatus.COMPLETED;
                    download.updatedAt = new Date();

                    const response = {
                        update : true,
                        download: download
                    };

                    webSocket.send(JSON.stringify(response));
                }).catch((err) => {
                    console.error(err);
                    download.status = downloadStatus.FAILED;
                    download.updatedAt = new Date();

                    const response = {
                        update : true,
                        download: download
                    };

                    webSocket.send(JSON.stringify(response));
                });
            }
            ).catch((err) => {
                console.error(err);
                download.status = downloadStatus.UNBLOCKINGFAILED;
                download.updatedAt = new Date();

                const response = {
                    update : true,
                    download: download
                };

                webSocket.send(JSON.stringify(response));
            });


        }).catch((err) => {
            console.error(err);
            download.status = downloadStatus.BYPASSINGPROTECTIONFAILED;
            download.updatedAt = new Date();

            const response = {
                update : true,
                download: download
            };

            webSocket.send(JSON.stringify(response));
        });
    }).catch((err) => {
        console.error(err);
        download.status = downloadStatus.ERROR;
        download.updatedAt = new Date();

        const response = {
            update : true,
            download: download
        };

        webSocket.send(JSON.stringify(response));
    });
});

app.get('/downloads', (req, res) => {
    res.json(downloads);
});

app.ws('/downloads', function (ws, req) {
    webSocket = ws;
});

app.listen(3000, () => {
    console.log('Server is running on port 3000');
});