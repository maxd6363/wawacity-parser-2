import { Injectable } from '@angular/core';
import { Item } from 'src/models/items.model';
import { WawacityItemsSearchService } from '../services/wawacity-items-search.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemsStore {
  private items: Subject<Item[]> = new Subject();
  items$ = this.items.asObservable();

  constructor(private itemsSearchService: WawacityItemsSearchService) { }

  async search(searchValue: string) {
    this.itemsSearchService.searchItems(searchValue).subscribe((items) => {
      this.items.next(items);
    });
  }
}
