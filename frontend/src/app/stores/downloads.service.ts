import { Injectable } from '@angular/core';
import { Download } from 'src/models/download.model';
import { ApiDownloadsService } from '../services/api-downloads.service';
import { Item } from 'src/models/items.model';
import { WebSocketService } from '../services/web-socket.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DownloadsStore {

  downloads$ = new BehaviorSubject<Download[]>([]);

  constructor(private downloadApi: ApiDownloadsService, private webSocket: WebSocketService) {
    webSocket.messageReceived$.subscribe((message) => {
      if (message.new) {
        console.log("WS: New download", message.download);
        this.downloads$.next([...this.downloads$.value, message.download]);
      }
      if (message.update) {
        console.log("WS: Download updated", message.download);
        this.downloads$.next(this.downloads$.value.map((download) => {
          if (download.id === message.download.id) {
            return message.download;
          }
          return download;
        }));
      }
    });
  }

  downloadItem(item: Item) {
    this.downloadApi.downloadItem(item);
  }
}
