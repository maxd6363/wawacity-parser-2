import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  private socket: WebSocket = new WebSocket(`${environment.apiUrl}downloads`.replace('http', 'ws'));

  messageReceived$ = new Subject<any>();

  constructor() {
    this.connect();
  }

  connect(): void {
    this.socket.onopen = () => {
      console.log('WebSocket connection established.');
    };

    this.socket.onmessage = (event) => {
      console.log('Received message:', event.data);
      this.messageReceived$.next(JSON.parse(event.data));
    };

    this.socket.onclose = (event) => {
      console.log('WebSocket connection closed:', event);
    };

    this.socket.onerror = (error) => {
      console.error('WebSocket error:', error);
    };
  }

  sendMessage(message: string): void {
    console.log('Sending message:', message);

    this.socket.send(message);
  }

  closeConnection(): void {
    this.socket.close();
  }
}
