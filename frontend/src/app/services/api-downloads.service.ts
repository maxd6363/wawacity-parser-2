import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environment/environment';
import { Download } from 'src/models/download.model';
import { Item } from 'src/models/items.model';

@Injectable({
  providedIn: 'root'
})
export class ApiDownloadsService {

  constructor(private http : HttpClient) { }

  downloadItem(item : Item){
    try {
      this.http.post(`${environment.apiUrl}download/`, item).subscribe();
    } catch (error) {
      console.error(error);
    }
  }
}
