import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';
import { Item } from 'src/models/items.model';

@Injectable({
  providedIn: 'root'
})
export class WawacityItemsSearchService {
  apiUrl = environment.apiUrl;

  constructor(private http : HttpClient) { }

  searchItems(searchValue: string) : Observable<Item[]> {
    try {
      return this.http.get<Item[]>(`${this.apiUrl}search?search=${searchValue}&category=films`);
    } catch (error) {
      console.error(error);
      return new Observable<Item[]>();
    }
  }
}
