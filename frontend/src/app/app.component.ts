import { Component } from '@angular/core';
import { Subject, debounceTime } from 'rxjs';
import { ItemsStore } from './stores/items.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Downloader';
  searchValue: string = "";
  downloadPage: boolean = false;

  private searchSubject = new Subject<string>();

  constructor(private itemsService: ItemsStore) { }

  ngOnInit() {
    // let value = "star wars";
    this.searchSubject.pipe(debounceTime(800)).subscribe((value) => {
      console.log("Searching for ", value);
      this.itemsService.search(value);
    });
  }

  search() {
    this.searchSubject.next(this.searchValue);
  }

  ngOnDestroy() {
    this.searchSubject.unsubscribe();
  }
}
