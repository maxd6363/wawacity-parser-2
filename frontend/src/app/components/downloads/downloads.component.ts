import { Component } from '@angular/core';
import { DownloadsStore } from 'src/app/stores/downloads.service';
import { DownloadStatus } from 'src/models/download-status.enum';
import { Download } from 'src/models/download.model';

@Component({
  selector: 'app-downloads',
  templateUrl: './downloads.component.html',
  styleUrls: ['./downloads.component.css']
})
export class DownloadsComponent {
  downloads: Download[] = [];

  constructor(private downloadStore: DownloadsStore) { }

  ngOnInit() {
    this.downloadStore.downloads$.subscribe((downloads) => {
      this.downloads = downloads;
    });
  }

  getColor(status: DownloadStatus) {
    switch (status) {
      case DownloadStatus.DOWNLOADING:
        return "text-bg-primary";
      case DownloadStatus.COMPLETED:
      case DownloadStatus.STARTING:
      case DownloadStatus.GETTINGLINKS:
        return "text-bg-success";
      case DownloadStatus.FAILED:
      case DownloadStatus.BYPASSINGPROTECTIONFAILED:
      case DownloadStatus.UNBLOCKINGFAILED:
        return "text-bg-danger";
      case DownloadStatus.UNKNOWN:
        return "text-bg-secondary";
      case DownloadStatus.BYPASSINGPROTECTION:
      case DownloadStatus.UNBLOCKING:
        return "text-bg-info";
      default:
        return "text-bg-info";
    }
  }
}
