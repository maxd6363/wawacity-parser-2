import { Component } from '@angular/core';
import { ItemsStore } from 'src/app/stores/items.service';
import { Item } from 'src/models/items.model';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent {
  items : Item[] = [];

  constructor(private itemsStore : ItemsStore) { }

  ngOnInit() {
    this.itemsStore.items$.subscribe((items) => {
      this.items = items;
    });
  }
}
