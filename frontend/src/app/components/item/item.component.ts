import { Component, Input } from '@angular/core';
import { DownloadsStore } from 'src/app/stores/downloads.service';
import { Item } from 'src/models/items.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent {
  @Input() item!: Item;

  constructor (private downloadStore : DownloadsStore) { }

  download() {
    this.downloadStore.downloadItem(this.item);
  }

}
