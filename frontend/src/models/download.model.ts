import { DownloadStatus } from "./download-status.enum";

export class Download {
  id: number = 0;
  url: string = "";
  title: string = "";
  quality: string = "";
  status: DownloadStatus = DownloadStatus.UNKNOWN;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
  progress: number = 0;
  size: string = "";
}
