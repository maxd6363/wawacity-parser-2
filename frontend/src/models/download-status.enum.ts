export enum DownloadStatus {
  UNKNOWN = "Unknown",
  STARTING = "Starting",
  GETTINGLINKS = "Getting Links",
  BYPASSINGPROTECTION = "Bypassing Protection",
  BYPASSINGPROTECTIONFAILED = "Bypassing Protection Failed",
  UNBLOCKING = "Unblocking",
  UNBLOCKINGFAILED = "Unblocking Failed",
  DOWNLOADING = "Downloading",
  COMPLETED = "Completed",
  FAILED = "Failed"
}

export const DownloadStatusArray = Object.keys(DownloadStatus).map(key => DownloadStatus[key as keyof typeof DownloadStatus]);
