export class Item {
  title: string;
  year: string;
  url: string;
  quality: string;
  image: string;
  language: string;

  constructor(title: string, year: string, url: string, quality: string, image: string, language: string) {
    this.title = title;
    this.year = year;
    this.url = url;
    this.quality = quality;
    this.image = image;
    this.language = language;
  }
}
